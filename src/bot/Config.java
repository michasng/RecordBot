package bot;


import java.awt.event.KeyEvent;
import java.io.*;
import java.util.LinkedList;
import java.util.Properties;

public class Config {

  private final String base_dir;
  private Observable<String> directory;
  private boolean longPressAsOne;

  public Config(String base_dir) {
    this.base_dir = base_dir;

    Properties prop = new Properties();
    InputStream input = null;
    try {
      input = new FileInputStream(base_dir + File.separatorChar + "config.properties");
      prop.load(input);
      input.close();
    } catch (FileNotFoundException e) {
      // no properties file yet
    } catch (IOException e) {
      e.printStackTrace();
    }

    String directory = prop.getProperty("directory");
    if (directory == null) {
      directory = base_dir;
      prop.setProperty("directory", directory);
    }
    this.directory = new Observable<String>(directory);

    String lpaoString = prop.getProperty("longPressAsOne");
    if (lpaoString == null)
      longPressAsOne = true;
    else
      longPressAsOne = Boolean.valueOf(lpaoString);

    save();
  }

  public void save() {
    Properties prop = new Properties();
    prop.setProperty("directory", directory.getValue());
    prop.setProperty("longPressAsOne", String.valueOf(longPressAsOne));
    OutputStream output = null;
    try {
      output = new FileOutputStream(base_dir + File.separatorChar + "config.properties");
      prop.store(output, null);
      output.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void setLongPressAsOne(boolean value) {
    longPressAsOne = value;
    save();
  }

  public boolean isLongPressAsOne() {
    return longPressAsOne;
  }

  public void setDirectory(String directory) {
    this.directory.setValue(directory);
    save();
  }

  public String getDirectory() {
    return directory.getValue();
  }

  public void subscribeDirectory(Observable.Observer<String> observer) {
    directory.subscribe(observer);
  }

  public void unsubscribeDirectory(Observable.Observer<String> observer) {
    directory.unsubscribe(observer);
  }

  public LinkedList<Integer> getRecordKeys() {
    LinkedList<Integer> recordKeys = new LinkedList<>();
    recordKeys.add(KeyEvent.VK_CONTROL);
    recordKeys.add(KeyEvent.VK_ALT);
    recordKeys.add(KeyEvent.VK_R);
    return recordKeys;
  }

  public LinkedList<Integer> getEscapeKeys() {
    LinkedList<Integer> escapeKeys = new LinkedList<>();
    escapeKeys.add(KeyEvent.VK_CONTROL);
    escapeKeys.add(KeyEvent.VK_ALT);
    escapeKeys.add(KeyEvent.VK_E);
    return escapeKeys;
  }

}
