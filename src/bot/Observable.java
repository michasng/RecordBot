package bot;

import java.util.LinkedList;

public class Observable<T> {

  private T value;
  private LinkedList<Observer<T>> observers;

  public Observable() {
    observers = new LinkedList<>();
  }

  public Observable(T value) {
    this();
    this.value = value;
  }

  public void setValue(T value) {
    this.value = value;
    notifyObservers();
  }

  public T getValue() {
    return value;
  }

  public void subscribe(Observer<T> observer) {
    this.observers.add(observer);
  }

  public void unsubscribe(Observer<T> observer) {
    this.observers.remove(observer);
  }

  public boolean hasObservers() {
    return !observers.isEmpty();
  }

  public void notifyObservers() {
    for (Observer<T> observer : observers)
      observer.update(value);
  }

  public static interface Observer<T> {
    void update(T value);
  }

}
