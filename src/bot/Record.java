package bot;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Record {

  public static LinkedList<Record> getRecords(String directory) {
    new File(directory).mkdirs();
    LinkedList<Record> records = new LinkedList<>();

    // all files in the record-directory of type .json with a corresponding
    // .png file
    File[] files = new File(directory).listFiles(file -> file.isFile() && file.getName().length() > 4
      && file.getName().substring(file.getName().length() - 5).equalsIgnoreCase(".json")
      && new File(file.getPath().substring(0, file.getPath().length() - 4) + "png").exists());
    System.out.println("Found " + files.length + " records");

    for (File file : files) {
      String path = file.getPath().substring(0, file.getPath().length() - 5);
      try {
        InputStream stream = new FileInputStream(file);
        JsonObject recordJson = JsonObject.readFrom(new InputStreamReader(stream));
        File imgFile = new File(path + ".png");
        if (!imgFile.exists())
          continue;
        Image image = SwingFXUtils.toFXImage(ImageIO.read(imgFile), null);
        records.add(new Record(recordJson, image, path));

        stream.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    System.out.println(records.size() + " records have been loaded");

    return records;
  }

  private String name;
  private double condition;
  private int x, y, width, height;
  private Image image;
  private LinkedList<UserAction> userActions;

  private String path;

  public Record(Image image, String folder) {
    this.image = image;
    userActions = new LinkedList<>();

    // set default values
    this.condition = 0.95;
    this.x = 0;
    this.y = 0;
    this.width = (int) image.getWidth();
    this.height = (int) image.getHeight();

    int number = 0;
    File file = null;
    do {
      number++;
      this.name = "record" + number;
      file = new File(folder + File.separatorChar + name + ".json");
    } while (file.exists());
    this.path = folder + File.separatorChar + name;

    // pre-determine name, condition and dimensions
  }

  private Record(JsonObject recordJson, Image image, String path) {
    this.image = image;
    // basically split, bc path.split(File.separator)[0]; throws a
    // PatternSyntaxException
    int nameLength = 0;
    for (int i = path.length() - 1; i >= 0; i--)
      if (path.charAt(i) == File.separatorChar)
        break;
      else
        nameLength++;
    this.name = path.substring(path.length() - nameLength, path.length());
    this.path = path;
    this.condition = recordJson.get("condition").asDouble();
    this.x = recordJson.get("x").asInt();
    this.y = recordJson.get("y").asInt();
    this.width = recordJson.get("width").asInt();
    this.height = recordJson.get("height").asInt();
    userActions = new LinkedList<>();
    JsonArray userActionsJson = recordJson.get("userActions").asArray();
    for (JsonValue userActionJson : userActionsJson)
      userActions.add(new UserAction(userActionJson.asObject()));
  }

  public double getSimilarity(Image comparisson) {
    // scale-factor to multiply the record's size with, to get
    // the current screen-size
    double xScale = comparisson.getWidth() / image.getWidth();
    double yScale = comparisson.getHeight() / image.getHeight();
    // System.out.println("record: " + image.getWidth() + ", " +
    // image.getHeight());
    // System.out.println("selection: " + x + ", " + y + ", " + width + ", "
    // + height);
    // System.out.println("screen: " + comparisson.getWidth() + ", " +
    // comparisson.getHeight());
    // System.out.println("scale: " + xScale + ", " + yScale);
    PixelReader imgReader = image.getPixelReader();
    PixelReader cmpReader = comparisson.getPixelReader();
    long totalDiff = 0; // sum of the difference of all pixels
    // loop through all pixels of the marked region
    for (int x = this.x; x < this.x + this.width; x++)
      for (int y = this.y; y < this.y + this.height; y++) {
        int rgb1 = imgReader.getArgb(x, y);
        // get the pixel to compare it with
        int rgb2 = cmpReader.getArgb((int) (x * xScale), (int) (y * yScale));
        totalDiff += Math.abs(((rgb1 & 0xff0000) >> 16) - ((rgb2 & 0xff0000) >> 16)); // add
        // red
        // difference
        totalDiff += Math.abs(((rgb1 & 0x00ff00) >> 8) - ((rgb2 & 0x00ff00) >> 8)); // add
        // green
        // difference
        totalDiff += Math.abs((rgb1 & 0x0000ff) - (rgb2 & 0x0000ff)); // add
        // blue
        // difference
      }
    // average difference between two pixels (from 0 to 3*255 = 765)
    double averageDiff = (double) totalDiff / (width * height);

    double percentDiff = averageDiff / (3 * 255); // value between 0 and 1
    // return percentage of similarity, not difference
    return 1 - percentDiff;
  }

  /**
   * Saves the record in the directory specified by it*s path. Overrides any
   * existing files with the same path.
   */
  public void save() {
    JsonArray userActionsJson = new JsonArray();
    for (UserAction userAction : userActions)
      userActionsJson.add(userAction.asJson());
    JsonObject record = new JsonObject().add("condition", condition).add("x", x).add("y", y).add("width", width)
      .add("height", height).add("userActions", userActionsJson);
    try {
      new File(path).getParentFile().mkdirs();

      FileWriter fStream = new FileWriter(path + ".json");
      BufferedWriter out = new BufferedWriter(fStream);
      record.writeTo(out);
      out.close();

      File imgFile = new File(path + ".png");
      BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
      ImageIO.write(bImage, "png", imgFile);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Cuts the last two UserActions from the record, because they are just
   * pressing and releasing the stop-recording-button
   */
  public void finalCut() {
    userActions.removeLast();
    userActions.removeLast();
  }

  public void addAction(UserAction userAction) {
    userActions.add(userAction);
  }

  public Iterator<UserAction> actionsInterator() {
    return userActions.iterator();
  }

  public void setUserActions(List<UserAction> userActions) {
    this.userActions = new LinkedList<>();
    for (UserAction userAction : userActions) {
      this.userActions.add(userAction);
    }
  }

  public void setCondition(double condition) {
    this.condition = condition;
  }

  public void setBounds(int x, int y, int width, int height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  public String getName() {
    return name;
  }

  public double getCondition() {
    return condition;
  }

  public Image getImage() {
    return image;
  }

  public String getPath() {
    return path;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

}
