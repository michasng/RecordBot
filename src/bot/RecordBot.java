package bot;

import bot.states.MainState;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

import java.awt.*;
import java.awt.event.InputEvent;
import java.io.File;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RecordBot extends Application implements StateContext {

  public static final String BASE_DIR = System.getProperty("user.home") + File.separator + "documents" + File.separator
    + "RecordBot";

  private static final int MOUSE_LEFT = InputEvent.BUTTON1_MASK, MOUSE_MIDDLE = InputEvent.BUTTON2_MASK,
    MOUSE_RIGHT = InputEvent.BUTTON3_MASK;

  private Config config;
  private int width, height;
  private StackPane stackPane;

  private State state;
  private Robot robot;
  private LinkedList<Record> records;

  private Stage stage;
  private int btnSize;

  public RecordBot() {
    config = new Config(BASE_DIR);
    try {
      robot = new Robot();
    } catch (AWTException e) {
      e.printStackTrace();
    }
    reloadRecords(); // ToDo: load current dir from a config file
    config.subscribeDirectory((directory) -> reloadRecords());

    // Get the logger for "org.jnativehook" and set the level to off.
    Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
    logger.setLevel(Level.OFF);
    // Don't forget to disable the parent handlers.
    logger.setUseParentHandlers(false);
    try {
      GlobalScreen.registerNativeHook();
    } catch (NativeHookException e) {
      e.printStackTrace();
      System.exit(1);
    }

  }

  @Override
  public void start(Stage primaryStage) {
    this.stage = primaryStage;

    width = (int) Screen.getPrimary().getBounds().getMaxX();
    height = (int) Screen.getPrimary().getBounds().getMaxY();
    btnSize = width / 24;

    stackPane = new StackPane();
    stackPane.setPrefSize(width, height);
    Scene scene = new Scene(stackPane, null);

    primaryStage.setTitle("bot"); // invisible anyway
    primaryStage.initStyle(StageStyle.TRANSPARENT);
    primaryStage.setAlwaysOnTop(true);

    scene.getStylesheets().add("style.css");
    primaryStage.setScene(scene);
    primaryStage.sizeToScene();
    primaryStage.setMaximized(true);
    primaryStage.setResizable(false);
    primaryStage.show();

    // start at MainState
    setState(new MainState(this));
  }

  public void setState(final State state) {
    if (this.state != null) {
      GlobalScreen.removeNativeMouseListener(this.state);
      GlobalScreen.removeNativeKeyListener(this.state);
      this.state.destroy();
    }
    this.state = state;
    GlobalScreen.addNativeMouseListener(state);
    GlobalScreen.addNativeKeyListener(state);

    Platform.runLater(() -> {
      // setting the root would glitch out the transparency
      // a stackPane also allows for a universal background (or none in this case)
      stackPane.getChildren().clear();
      stackPane.getChildren().add(state.component());

      new Thread(() -> state.init()).start();
    });

  }

  public void shutDown() {
    GlobalScreen.removeNativeMouseListener(this.state);
    GlobalScreen.removeNativeKeyListener(this.state);
    try { // get rid of the global input listener
      GlobalScreen.unregisterNativeHook();
    } catch (NativeHookException e) {
      e.printStackTrace();
    }
    Platform.exit();
  }

  public WritableImage takeScreenShot() {
    return SwingFXUtils.toFXImage(robot.createScreenCapture(new Rectangle(0, 0, width, height)), null);
  }

  @Override
  public void reloadRecords() {
    records = Record.getRecords(config.getDirectory());
  }

  @Override
  public void setAlwaysOnTop(boolean value) {
    stage.setAlwaysOnTop(value);
  }

  @Override
  public Robot getRobot() {
    return robot;
  }

  @Override
  public LinkedList<Record> getRecords() {
    return records;
  }

  @Override
  public Config getConfig() {
    return config;
  }

  @Override
  public int getBtnSize() {
    return btnSize;
  }

  @Override
  public int getWidth() {
    return width;
  }

  @Override
  public int getHeight() {
    return height;
  }

  public static int toRobotMouseBtn(int nativeMouseBtn) {
    switch (nativeMouseBtn) {
      case 1:
      case 16: // pen
        return MOUSE_LEFT;
      case 2:
        return MOUSE_RIGHT;
      case 3:
        return MOUSE_MIDDLE; // ? not sure
      default:
        System.out.println("Unknown mouse button " + nativeMouseBtn);
        return MOUSE_LEFT;
    }
  }

  public static void main(String[] args) {
    Application.launch(args);
  }

}
