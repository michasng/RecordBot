package bot;

import java.awt.event.KeyEvent;

import org.jnativehook.keyboard.SwingKeyAdapter;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;

import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public abstract class State extends SwingKeyAdapter implements NativeMouseListener {

  protected final StateContext context;

  public State(StateContext context) {
    this.context = context;
  }

  /**
   * Is called after the ui for the state is done loading
   */
  public abstract void init();

  public abstract void destroy();

  public abstract Parent component();

  protected Button createButton(Image image, EventHandler<? super MouseEvent> handler, double btnSize) {
    ImageView imageView = new ImageView(image);
    imageView.setFitWidth(btnSize);
    imageView.setFitHeight(btnSize);
    Button button = new Button(null, imageView);
    button.setMaxSize(btnSize, btnSize);
    button.setMinSize(btnSize, btnSize);
    if (handler == null)
      button.setDisable(true);
    else
      button.setOnMouseClicked(handler);
    return button;

  }

  protected Button createButton(String iconPath, EventHandler<? super MouseEvent> handler) {
    return createButton(new Image(this.getClass().getResourceAsStream(iconPath)), handler, context.getBtnSize());
  }

  public void showError(String title, String message) {
    System.err.println(title);
    context.setAlwaysOnTop(false);
    Alert alert = new Alert(Alert.AlertType.ERROR, message);
    alert.setTitle(title);
    alert.showAndWait();
    context.setAlwaysOnTop(true);
  }

  @Override
  public void nativeMousePressed(NativeMouseEvent nativeMouseEvent) {
    // do nothing yet
  }

  @Override
  public void nativeMouseReleased(NativeMouseEvent nativeMouseEvent) {
    // do nothing yet
  }

  @Override
  public void nativeMouseClicked(NativeMouseEvent nativeMouseEvent) {
    // already handled by pressed & released
  }

  @Override
  public void keyPressed(KeyEvent keyEvent) {
    // do nothing yet
  }

  @Override
  public void keyReleased(KeyEvent keyEvent) {
    // do nothing yet
  }

  @Override
  public final void keyTyped(KeyEvent keyEvent) {
    // doesn't seem to work, but already handled by
    // pressed & released anyway
  }

}
