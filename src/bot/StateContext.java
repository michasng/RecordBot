package bot;

import javafx.scene.image.WritableImage;

import java.awt.*;
import java.util.LinkedList;

public interface StateContext {

  void setState(State state);

  void shutDown();

  WritableImage takeScreenShot();

  Robot getRobot();

  void reloadRecords();

  LinkedList<Record> getRecords();

  void setAlwaysOnTop(boolean value);

  Config getConfig();

  int getBtnSize();

  int getWidth();

  int getHeight();

}
