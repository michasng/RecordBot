package bot;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import java.util.Arrays;

public class UserAction {

  public static final String MOUSE = "MOUSE", KEY = "KEY";
  public static final String PRESS = "PRESS", RELEASE = "RELEASE";

  private long delay;
  private String device;
  private String type;
  private int[] metaInf;

  public UserAction(long delay, String device, String type, int... metaInf) {
    this.delay = delay;
    this.device = device;
    this.type = type;
    this.metaInf = metaInf;
  }

  public UserAction(JsonObject userActionJson) {
    this.delay = userActionJson.get("delay").asLong();
    this.device = userActionJson.get("device").asString();
    this.type = userActionJson.get("type").asString();
    JsonArray metaInfJson = userActionJson.get("metaInf").asArray();
    this.metaInf = new int[metaInfJson.size()];
    for (int i = 0; i < metaInfJson.size(); i++)
      metaInf[i] = metaInfJson.get(i).asInt();
  }

  public JsonObject asJson() {
    JsonArray metaInfJson = new JsonArray();
    for (Integer inf : metaInf)
      metaInfJson.add(inf);
    return new JsonObject().add(
      "delay", delay).add(
      "device", device).add(
      "type", type).add("metaInf", metaInfJson);
  }

  public void setDelay(long delay) {
    this.delay = delay;
  }

  public void setDevice(String device) {
    if (device.equals(MOUSE) || device.equals(KEY))
      this.device = device;
  }

  public void setType(String type) {
    if (type.equals(PRESS) || type.equals(RELEASE))
      this.type = type;
  }

  public void setMetaInf(int[] metaInf) {
    this.metaInf = metaInf;
  }

  public long getDelay() {
    return delay;
  }

  public String getDevice() {
    return device;
  }

  public String getType() {
    return type;
  }

  public int[] getMetaInf() {
    return metaInf;
  }

  public boolean deviceEquals(String device) {
    return this.device.equals(device);
  }

  public boolean typeEquals(String type) {
    return this.type.equals(type);
  }

  @Override
  public UserAction clone() {
    return new UserAction(delay, device, type, metaInf);
  }

  public boolean isValid() {
    return delay >= 0 && (type.equals(PRESS) || type.equals(RELEASE)) && metaInf != null &&
      ((device.equals(MOUSE) && metaInf.length == 3) || (device.equals(KEY) && metaInf.length == 1));
  }

  @Override
  public String toString() {
    return "delay: " + delay + ", device: " + device + ", type: " + type + ", metaInf: " + Arrays.toString(metaInf);
  }

}
