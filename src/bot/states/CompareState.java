package bot.states;

import bot.Record;
import bot.State;
import bot.StateContext;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import org.jnativehook.GlobalScreen;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseMotionListener;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.Random;

public class CompareState extends State implements Runnable, NativeMouseMotionListener {

  private boolean running;
  private Label label;
  private WritableImage image;
  private ImageView currentImage;
  private ImageView bestImage;
  private HBox options;
  private VBox container;
  private Rectangle hideRect;

  private LinkedList<Record> condRecords;
  private LinkedList<Record> defaultRecords;

  private LinkedList<Integer> pressedKeys;
  private LinkedList<Integer> escapeKeys;

  public CompareState(StateContext context) {
    super(context);
  }

  @Override
  public void init() {
    condRecords = new LinkedList<>();
    defaultRecords = new LinkedList<>();
    for (Record record : context.getRecords())
      if (record.getCondition() == 0)
        defaultRecords.add(record);
      else
        condRecords.add(record);

    pressedKeys = new LinkedList<>();
    escapeKeys = context.getConfig().getEscapeKeys();

    GlobalScreen.addNativeMouseMotionListener(this);
    run();
  }

  @Override
  public void destroy() {
    running = false;
    GlobalScreen.removeNativeMouseMotionListener(this);
  }

  @Override
  public Parent component() {
    options = new HBox();
    options.setPrefSize(context.getBtnSize() * 3, context.getBtnSize());
    options.setAlignment(Pos.TOP_RIGHT);

    label = new Label();
    label.setPrefSize(4 * context.getBtnSize(), context.getBtnSize());
    label.setAlignment(Pos.CENTER);
    options.getChildren().add(label);

    Button button = createButton("/icons/pause.png", (event) -> context.setState(new MainState(context)));
    options.getChildren().add(button);

    options.getChildren().add(createButton("/icons/record_disabled.png", null));
    options.getChildren().add(createButton("/icons/options_disabled.png", null));

    container = new VBox();
    container.setPrefSize(context.getWidth(), context.getHeight());
    container.setAlignment(Pos.TOP_RIGHT);
    container.getChildren().add(options);
    currentImage = new ImageView();
    currentImage.setFitWidth(context.getWidth() / 4);
    currentImage.setFitHeight(context.getHeight() / 4);
    container.getChildren().add(currentImage);
    bestImage = new ImageView();
    bestImage.setFitWidth(context.getWidth() / 4);
    bestImage.setFitHeight(context.getHeight() / 4);
    container.getChildren().add(bestImage);
    hideRect = new Rectangle(3 * context.getWidth() / 4, 0, context.getWidth() / 4, context.getHeight() / 3);

    return container;
  }

  @Override
  public void run() {
    running = true;

    while (running) {

      image = context.takeScreenShot();

      Record bestRecord = findBestRecord(image);
      if (bestRecord != null) {
        GlobalScreen.removeNativeMouseMotionListener(this);
        context.setState(new PlayState(context, bestRecord));
        return; // no need to set running to false then
      }
      // Platform.runLater(() -> currentImage.setImage(image));

      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  ///*
  private Image subImage(Image image, int x, int y, int width, int height) {
    BufferedImage bImg = SwingFXUtils.fromFXImage(image, null);
    System.out.println("sub: " + x + ", " + y + ", " + width + ", " + height);
    System.out.println("bimg: " + bImg.getWidth() + ", " + bImg.getHeight());
    return SwingFXUtils.toFXImage(bImg.getSubimage(x, y, width, height), null);
  }
  //*/

  /**
   * @param image The image to compare to all of the records'
   * @return the best fitting recording, null if there are no records.
   * Ignores the condition of the record being or not being met
   */
  private Record findBestRecord(Image image) {
    double maxSimilarity = 0;
    Record bestRecord = null;
    for (Record record : condRecords) {
      double currentSimilarity = record.getSimilarity(image);
      if (currentSimilarity >= maxSimilarity) {
        maxSimilarity = currentSimilarity;
        bestRecord = record;
      }
    }

    if (bestRecord == null)
      return null;

    final double finSim = maxSimilarity;
    final Record finBestRec = bestRecord;
    Platform.runLater(() -> {
      label.setText("Best: " + (int) (100 * finSim) / 100.0);
      // /*
      bestImage.setImage(subImage(finBestRec.getImage(), finBestRec.getX(), finBestRec.getY(),
        finBestRec.getWidth(), finBestRec.getHeight()));
      // scale-factor to multiply the best-record-image's size with, to get the screen-size
      double xScale = image.getWidth() / finBestRec.getImage().getWidth();
      double yScale = image.getHeight() / finBestRec.getImage().getHeight();
      currentImage.setImage(subImage(image, (int) (finBestRec.getX() * xScale), (int) (finBestRec.getY() * yScale),
        (int) (finBestRec.getWidth() * xScale), (int) (finBestRec.getHeight() * yScale)));
      // */
      // currentImage.setImage(image);
    });
    System.out.println("Best: " + maxSimilarity);

    if (bestRecord.getCondition() > maxSimilarity)
      if (defaultRecords.isEmpty())
        return null;
      else
        bestRecord = defaultRecords.get(new Random().nextInt(defaultRecords.size()));

    return bestRecord;
  }

  @Override
  public void keyPressed(KeyEvent keyEvent) {
    if (!pressedKeys.contains(keyEvent.getKeyCode()))
      pressedKeys.add(keyEvent.getKeyCode());
    if (pressedKeys.containsAll(escapeKeys))
      context.setState(new MainState(context));
  }

  @Override
  public void keyReleased(KeyEvent keyEvent) {
    // remove as integer, bc int would be seen as an index
    pressedKeys.remove(new Integer(keyEvent.getKeyCode()));
  }

  @Override
  public void nativeMouseMoved(NativeMouseEvent event) {
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    double scaleX = context.getWidth() / screenSize.getWidth();
    double scaleY = context.getHeight() / screenSize.getHeight();
    // System.out.println(scaleX + ", " + scaleY);
    final double x = event.getX() * scaleX;
    final double y = event.getY() * scaleY;
    Platform.runLater(() -> container.setVisible(!options.contains(x, y)));
  }

  @Override
  public void nativeMouseDragged(NativeMouseEvent event) {
    // do nothing
  }

}
