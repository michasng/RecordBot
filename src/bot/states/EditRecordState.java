package bot.states;

import bot.Record;
import bot.State;
import bot.StateContext;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class EditRecordState extends State {

  private Record record;
  private String exitState;

  private TextField condition;
  private int startX, startY, endX, endY;

  private Canvas canvas;

  public EditRecordState(StateContext context, Record record, String exitState) {
    super(context);
    this.record = record;
    this.exitState = exitState;
  }

  @Override
  public void init() {
    condition.setText(String.valueOf(record.getCondition()));

    this.startX = record.getX();
    this.startY = record.getY();
    this.endX = record.getX() + record.getWidth();
    this.endY = record.getY() + record.getHeight();
    drawArea();
  }

  private void drawArea() {
    GraphicsContext gc = canvas.getGraphicsContext2D();
    gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

    // don't draw the selection, if the full image is selected
    if (startX == 0 && startY == 0 && endX == context.getWidth() && endY == context.getHeight())
      return;

    // scale-factor to multiply the record-image's size with, to get the screen-size
    double xScale = canvas.getWidth() / record.getImage().getWidth();
    double yScale = canvas.getHeight() / record.getImage().getHeight();

    gc.setFill(new Color(0.2, 0.2, 1, 0.3));
    // double scale = canvas.getWidth() / context.getWidth();
    gc.fillRect(Math.min(startX, endX) * xScale, Math.min(startY, endY) * yScale,
      Math.abs(endX - startX) * xScale, Math.abs(endY - startY) * yScale);
  }

  private void resetArea() {
    startX = 0;
    startY = 0;
    endX = context.getWidth();
    endY = context.getHeight();
  }

  @Override
  public void destroy() {
  }

  @Override
  public Parent component() {
    HBox hBox = new HBox();
    hBox.setPrefSize(context.getWidth(), context.getBtnSize());
    hBox.setAlignment(Pos.TOP_RIGHT);

    Label label = new Label("Set area and %");
    label.setPrefSize(context.getWidth() / 2 - context.getBtnSize() * 5, context.getBtnSize());
    label.setAlignment(Pos.CENTER);
    hBox.getChildren().add(label);

    hBox.getChildren().add(createButton("/icons/film.png",
      event -> context.setState(new EditUserActionsState(context, record, this))));

    condition = new TextField();
    condition.setPrefSize(context.getBtnSize() * 2, context.getBtnSize());
    hBox.getChildren().add(condition);

    hBox.getChildren().add(createButton("/icons/save.png", (event) -> {
      double condValue;
      try {
        condValue = Double.parseDouble(condition.getText());
      } catch (NumberFormatException e) {
        System.err.println("Please enter a number 0-1 as condition");
        return;
      }
      record.setCondition(condValue);

      record.setBounds(Math.min(startX, endX), Math.min(startY, endY),
        Math.abs(endX - startX), Math.abs(endY - startY));
      record.save();

      context.reloadRecords();
      context.setState(getExitState());
    }));
    hBox.getChildren().add(createButton("/icons/cancel.png", (event) -> context.setState(getExitState())));

    VBox vBox = new VBox();
    vBox.setPrefSize(context.getWidth(), context.getHeight());
    vBox.setAlignment(Pos.TOP_RIGHT);
    vBox.getChildren().add(hBox);
    vBox.getChildren().add(getSecondRow());

    return vBox;
  }

  private State getExitState() {
    if (exitState.equals(MainState.class.getName()))
      return new MainState(context);
    else if (exitState.equals(FolderState.class.getName()))
      return new FolderState(context);
    else throw new RuntimeException("Invalid state");
  }

  private Node getSecondRow() {
    double scale = 0.5;
    int width = (int) (context.getWidth() * scale);
    int height = (int) (context.getHeight() * scale);

    ImageView imageView = new ImageView(record.getImage());
    imageView.setFitWidth(width);
    imageView.setFitHeight(height);
    canvas = new Canvas();
    canvas.setWidth(width);
    canvas.setHeight(height);
    canvas.setOnMousePressed(event -> {
      startX = (int) (event.getX() / scale);
      startY = (int) (event.getY() / scale);
    });
    canvas.setOnMouseDragged(event -> {
      endX = (int) (event.getX() / scale);
      endY = (int) (event.getY() / scale);
      drawArea();
    });
    canvas.setOnMouseReleased(event -> {
      endX = (int) (event.getX() / scale);
      endY = (int) (event.getY() / scale);
      // if the selection is too small in both directions
      if (Math.abs(endX - startX) < context.getWidth() / 20
        && Math.abs(endY - startY) < context.getHeight() / 20)
        resetArea();
      drawArea();
    });
    StackPane stackPane = new StackPane();
    stackPane.setAlignment(Pos.TOP_RIGHT);
    stackPane.setPrefWidth(width);
    stackPane.setPrefHeight(height);
    stackPane.getChildren().add(imageView);
    stackPane.getChildren().add(canvas);
    return stackPane;
  }

}
