package bot.states;

import bot.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class EditUserActionsState extends State {

  private Record record;
  private EditRecordState editRecordState;
  private Observable<Integer> currentAction;
  private ArrayList<UserAction> userActions;

  private TextField delay, metaInf;
  private ComboBox<String> device, type;
  private Canvas canvas;

  public EditUserActionsState(StateContext context, Record record, EditRecordState editRecordState) {
    super(context);

    this.record = record;
    this.editRecordState = editRecordState;

    // has to be done in the constructor
    currentAction = new Observable<>(0);
  }

  @Override
  public void init() {
    // don't modify the original userActions
    userActions = new ArrayList<>();
    Iterator<UserAction> iterator = record.actionsInterator();
    while (iterator.hasNext())
      userActions.add(iterator.next().clone());
    Platform.runLater(() -> currentAction.notifyObservers());
  }

  @Override
  public void destroy() {
        /*
         * no need to unsubscribe from currentAction, bc of garbage collector
		 * (currentAction is private, so it has no other subscribers)
		 */
    context.reloadRecords();
  }

  @Override
  public Parent component() {
    VBox vBox = new VBox();
    vBox.setPrefSize(context.getWidth(), context.getHeight());
    vBox.setAlignment(Pos.TOP_RIGHT);
    vBox.getChildren().add(getTop());
    vBox.getChildren().add(getSecondRow());
    return vBox;
  }

  private UserAction getCurrentAction() {
    if (currentAction.getValue() < 0 || currentAction.getValue() >= userActions.size())
      return null;
    else
      return userActions.get(currentAction.getValue());
  }

  private void draw() {
    GraphicsContext gc = canvas.getGraphicsContext2D();
    gc.clearRect(0, 0, record.getImage().getWidth(), record.getImage().getHeight());

    if (!device.getValue().equals(UserAction.MOUSE))
      return;

    int[] metaInf = getMetaInf();
    if (metaInf == null)
      return;

    double xScale = canvas.getWidth() / record.getImage().getWidth();
    double yScale = canvas.getHeight() / record.getImage().getHeight();
    gc.setStroke(Color.RED);
    int size = context.getBtnSize() / 2;
    gc.strokeRect((metaInf[0] - size) * xScale, (metaInf[1] - size) * yScale, size, size);
  }

  private int[] getMetaInf() {
    String[] metaInfStrings = this.metaInf.getText().split(", ");
    int[] metaInf = new int[metaInfStrings.length];
    try {
      for (int i = 0; i < metaInfStrings.length; i++)
        metaInf[i] = Integer.valueOf(metaInfStrings[i]);
    } catch (NumberFormatException e) {
      System.err.println("MetaInf invalid");
      return null;
    }
    return metaInf;
  }

  public Node getTop() {
    HBox hBox = new HBox();
    hBox.setMinSize(context.getWidth() / 2, context.getBtnSize());
    hBox.setMaxSize(hBox.getMinWidth(), hBox.getMinHeight());

    delay = new TextField();
    delay.setPrefSize(hBox.getMinWidth() / 4, hBox.getMinHeight());
    hBox.getChildren().add(delay);
    delay.setOnAction(event -> applyDelay());

    device = new ComboBox<>(FXCollections.observableArrayList(UserAction.MOUSE, UserAction.KEY));
    device.setMinSize(hBox.getMinWidth() / 4, hBox.getMinHeight());
    device.setMaxSize(device.getMinWidth(), device.getMinHeight());
    hBox.getChildren().add(device);
    device.setOnAction(event -> applyDevice());

    type = new ComboBox<>(FXCollections.observableArrayList(UserAction.PRESS, UserAction.RELEASE));
    type.setMinSize(hBox.getMinWidth() / 4, hBox.getMinHeight());
    type.setMaxSize(type.getMinWidth(), type.getMinHeight());
    hBox.getChildren().add(type);
    type.setOnAction(event -> applyType());

    metaInf = new TextField();
    metaInf.setPrefSize(hBox.getMinWidth() / 4, hBox.getMinHeight());
    hBox.getChildren().add(metaInf);
    metaInf.setOnAction(event -> applyMetaInf());

    currentAction.subscribe((value) -> {
      UserAction action = getCurrentAction();
      if (action == null) {
        device.setValue("");
        type.setValue("");
        delay.setText("");
        metaInf.setText("");
      } else {
        device.setValue(action.getDevice());
        type.setValue(action.getType());
        delay.setText(String.valueOf(action.getDelay()));
        String metaInfString = Arrays.toString(action.getMetaInf());
        metaInf.setText(metaInfString.substring(1, metaInfString.length() - 1));
      }

      draw();
    });

    return hBox;
  }

  private void applyAll() {
    applyDelay();
    // applyDevie(); // not required
    // applyType();
    applyMetaInf();
  }

  private void applyDelay() {
    UserAction action = getCurrentAction();
    if (action == null)
      return;

    long longDelay;
    try {
      longDelay = Long.parseLong(delay.getText());
      if (longDelay < 0)
        System.err.println("delay must be positive");
      else
        action.setDelay(longDelay);
    } catch (NumberFormatException e) {
      System.err.println("delay can only be a number");
    }
  }

  private void applyDevice() {
    UserAction action = getCurrentAction();
    if (action == null)
      return;

    action.setDevice(device.getValue());
  }

  private void applyType() {
    UserAction action = getCurrentAction();
    if (action == null)
      return;

    action.setType(type.getValue());
  }

  private void applyMetaInf() {
    UserAction action = getCurrentAction();
    if (action == null)
      return;

    action.setMetaInf(getMetaInf());
  }

  public Node getSecondRow() {
    HBox hBox = new HBox();
    hBox.setMinSize(context.getWidth() / 2 + context.getBtnSize(), context.getHeight() / 2);
    hBox.setMaxSize(hBox.getMinWidth(), hBox.getMinHeight());

    VBox buttons = new VBox();
    buttons.setMinSize(context.getBtnSize(), context.getHeight() / 2);
    buttons.setMaxSize(buttons.getMinWidth(), buttons.getMaxWidth());
    hBox.getChildren().add(buttons);

    buttons.getChildren().add(createButton("/icons/accept.png", event -> {
      applyAll();
      for (UserAction userAction : userActions)
        if (!userAction.isValid()) {
          showError("User Action invalid",
            "User Action at index " + currentAction.getValue() + " is not valid.");
          System.err.println("User Action invalid");
          return;
        }
      record.setUserActions(userActions);
      context.setState(editRecordState);
    }));
    buttons.getChildren().add(createButton("/icons/cancel.png", event -> {
      context.setState(editRecordState);
    }));
    buttons.getChildren().add(createButton("/icons/add.png", event -> {
      applyAll();
      userActions.add(currentAction.getValue(), new UserAction(0, "", ""));
      currentAction.notifyObservers();
    }));
    buttons.getChildren().add(createButton("/icons/left.png", event -> {
      applyAll();
      if (currentAction.getValue() > 0)
        currentAction.setValue(currentAction.getValue() - 1);
    }));
    buttons.getChildren().add(createButton("/icons/right.png", event -> {
      applyAll();
      if (currentAction.getValue() < userActions.size() - 1)
        currentAction.setValue(currentAction.getValue() + 1);
    }));
    buttons.getChildren().add(createButton("/icons/delete.png", event -> {
      if (getCurrentAction() != null) {
        userActions.remove(currentAction.getValue().intValue());
        currentAction.notifyObservers();
        System.out.println("Deleted UserAction at index " + currentAction.getValue());
        // if (currentAction.getValue() > 0)
        // currentAction.setValue(currentAction.getValue() - 1);
      }
    }));

    StackPane stackPane = new StackPane();
    stackPane.setMinSize(context.getWidth() / 2, context.getHeight() / 2);
    stackPane.setMaxSize(stackPane.getMinWidth(), stackPane.getMinHeight());

    ImageView imageView = new ImageView(record.getImage());
    imageView.setFitWidth(stackPane.getMinWidth());
    imageView.setFitHeight(stackPane.getMinHeight());
    stackPane.getChildren().add(imageView);

    canvas = new Canvas();
    canvas.setWidth(stackPane.getMinWidth());
    canvas.setHeight(stackPane.getMinHeight());
    stackPane.getChildren().add(canvas);
    hBox.getChildren().add(stackPane);

    return hBox;
  }

  @Override
  public void keyPressed(KeyEvent event) {
    System.out.println(event.getKeyCode());
  }
}
