package bot.states;

import bot.*;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class FolderState extends State implements Observable.Observer<String> {

  private boolean deleteMode;

  private Label title;
  private FlowPane files;
  private Button dirUp;

  public FolderState(StateContext context) {
    super(context);
  }

  @Override
  public void init() {
    context.getConfig().subscribeDirectory(this);
    refreshDir();
  }

  @Override
  public void destroy() {
    context.getConfig().unsubscribeDirectory(this);
  }

  @Override
  public Parent component() {
    HBox hBox = new HBox();
    hBox.setPrefSize(context.getWidth(), context.getBtnSize());
    hBox.setAlignment(Pos.TOP_RIGHT);
    hBox.getChildren().add(folderPane());
    hBox.getChildren().add(optionsPane());
    return hBox;
  }

  private Node folderPane() {
    VBox vBox = new VBox();
    vBox.setMinSize(context.getWidth() / 2 - 1.5 * context.getBtnSize(), context.getHeight() / 2);
    vBox.setMaxSize(vBox.getMinWidth(), vBox.getMinHeight());

    title = new Label();
    title.setAlignment(Pos.CENTER);
    title.setTextAlignment(TextAlignment.CENTER);
    title.setPrefSize(context.getWidth() / 2 - context.getBtnSize(), 1.5 * context.getBtnSize());
    vBox.getChildren().add(title);

    files = new FlowPane();
    files.setMinSize(vBox.getMinWidth(), vBox.getMinHeight() - title.getPrefHeight());
    files.setMaxSize(files.getMinWidth(), files.getMinHeight());
    vBox.getChildren().add(files);
    // cannot add drag events to files, multiple events would fire at once
    files.setId("file-pane");

    return vBox;
  }

  private Node optionsPane() {
    VBox options = new VBox();
    options.setMinSize(1.5 * context.getBtnSize(), context.getHeight() / 2);
    options.setMaxSize(options.getMinWidth(), options.getMinHeight());
    options.setStyle("-fx-background-color: #2d2d2d");
    options.setAlignment(Pos.TOP_CENTER);
    options.setSpacing(context.getBtnSize() / 4);
    options.setPadding(new Insets(context.getBtnSize() / 4));
    options.getChildren().add(createButton("/icons/accept.png",
      (event) -> context.setState(new OptionsState(context))));
    dirUp = createButton("/icons/dir_up.png", (event) -> {
      setDirectory(new File(getDirectory()).getParent());
    });
    dirUp.setOnDragOver((event) -> handleOnDragOver(event, dirUp));
    dirUp.setOnDragDropped((event) -> handleOnDragDropped(event, new File(getDirectory()).getParentFile()));
    options.getChildren().add(dirUp);
    options.getChildren().add(createButton("/icons/folder.png", (event) -> {
      TextInputDialog dialog = new TextInputDialog("folder");
      dialog.setTitle("Enter folder name");
      dialog.setHeaderText("Please enter a name for the folder");
      context.setAlwaysOnTop(false); // primary stage would cover the dialog
      dialog.showAndWait();
      if (dialog.getResult() != null) {
        new File(getDirectory() + '/' + dialog.getResult()).mkdir();
        refreshDir();
      }
      context.setAlwaysOnTop(true);
    }));

    ToggleButton delete = new ToggleButton(null, new ImageView(new Image(this.getClass().getResourceAsStream("/icons/delete.png"))));
    delete.setMinSize(context.getBtnSize(), context.getBtnSize());
    delete.setOnMouseClicked((event) -> deleteMode = delete.isSelected());
    options.getChildren().add(delete);

    return options;
  }

  public void update(String directory) {

    final File[] folders = new File(directory).listFiles(File::isDirectory);

    final LinkedList<Record> records = context.getRecords();

    Platform.runLater(() -> {
      title.setText(directory.substring(RecordBot.BASE_DIR.length()));
      // only enable going further up, while within the records folder
      dirUp.setDisable(new File(directory).equals(new File(RecordBot.BASE_DIR)));

      files.getChildren().clear();
      for (File folder : folders)
        files.getChildren().add(folderNode(folder));

      for (Record record : records)
        files.getChildren().add(recordNode(record));

    });
  }

  private void refreshDir() {
    context.reloadRecords();
    update(context.getConfig().getDirectory());
  }

  private void setDirectory(String directory) {
    context.getConfig().setDirectory(directory);
  }

  private String getDirectory() {
    return context.getConfig().getDirectory();
  }

  private Node recordNode(Record record) {
    File recordJson = new File(record.getPath() + ".json");
    File recordPng = new File(record.getPath() + ".png");

    Button button = createButton(record.getImage(), (event) -> {
      if (deleteMode) {
        context.setAlwaysOnTop(false);
        Alert alert = new Alert(AlertType.CONFIRMATION, "Are you sure you want to delete this record?");
        alert.showAndWait()
          .filter(response -> response == ButtonType.OK)
          .ifPresent(response -> {
            recordJson.delete();
            recordPng.delete();
            refreshDir();
          });
        context.setAlwaysOnTop(true);
      } else
        context.setState(new EditRecordState(context, record, getClass().getName()));
    }, 2 * context.getBtnSize());
    button.setOnDragDetected(event -> {
      // drag was detected, start a drag-and-drop gesture
      Dragboard db = button.startDragAndDrop(TransferMode.MOVE);

      // Put a file on a dragboard
      ClipboardContent content = new ClipboardContent();
      LinkedList<File> fileList = new LinkedList<>();
      fileList.add(recordJson);
      fileList.add(recordPng);
      content.putFiles(fileList);
      db.setContent(content);
    });

    VBox vBox = new VBox();
    vBox.setMaxSize(button.getMaxWidth(), button.getMaxHeight() * 1.5);
    vBox.setMinSize(vBox.getMinWidth(), vBox.getMinHeight());
    vBox.getChildren().add(button);
    TextField label = new TextField(record.getName());
    label.setMaxSize(vBox.getMaxWidth(), button.getMaxHeight() / 2);
    label.setMinSize(label.getMaxWidth(), label.getMinHeight());
    label.setStyle("-fx-font-size: " + label.getHeight() + "px");
    label.setAlignment(Pos.CENTER);
    label.setOnKeyPressed((event) -> {
      if (event.getCode() != KeyCode.ENTER)
        return;

      String newName = getDirectory() + File.separatorChar + label.getText();
      boolean success =
        recordJson.renameTo(new File(newName + ".json")) &&
          recordPng.renameTo(new File(newName + ".png"));
      if (!success)
        showError("Error renaming record",
          "Record could not be renamed to " + label.getText() + '.' + System.lineSeparator() +
            "Another record might have the same name or you are using invalid symbols.");

      refreshDir();
    });
    vBox.getChildren().add(label);

    return vBox;
  }

  private Node folderNode(File folder) {
    Button button = createButton("/icons/folder.png", (event) -> {
      if (deleteMode) {
        // hacky, but solves problem "variable must be final in lambda expression"
        final boolean[] sure = new boolean[1];
        context.setAlwaysOnTop(false);
        Alert alert = new Alert(AlertType.CONFIRMATION, "Are you sure you want to delete this folder?");
        alert.showAndWait()
          .filter(response -> response == ButtonType.OK)
          .ifPresent(response -> sure[0] = true);
        context.setAlwaysOnTop(true);

        if (sure[0]) {
          if (folder.delete())
            refreshDir();
          else
            showError("Error deleting folder", "You can only delete empty folders");
        }
      } else
        this.setDirectory(folder.getPath());
    });

    button.setOnDragDetected(event -> {
      Dragboard db = button.startDragAndDrop(TransferMode.MOVE);
      ClipboardContent content = new ClipboardContent();
      LinkedList<File> fileList = new LinkedList<>();
      fileList.add(folder);
      content.putFiles(fileList);
      db.setContent(content);
    });
    button.setOnDragOver((event) -> handleOnDragOver(event, button));
    button.setOnDragDropped((event) -> handleOnDragDropped(event, folder));

    VBox vBox = new VBox();
    vBox.setMaxSize(button.getMaxWidth(), button.getMaxHeight() * 1.5);
    vBox.setMinSize(vBox.getMinWidth(), vBox.getMinHeight());
    vBox.getChildren().add(button);
    TextField label = new TextField(folder.getName());
    label.setMaxSize(vBox.getMaxWidth(), button.getMaxHeight() / 2);
    label.setMinSize(label.getMaxWidth(), label.getMinHeight());
    label.setStyle("-fx-font-size: " + label.getHeight() + "px");
    label.setAlignment(Pos.CENTER);
    label.setOnKeyPressed((event) -> {
      if (event.getCode() != KeyCode.ENTER)
        return;

      boolean success = folder.renameTo(new File(folder.getParent() + File.separatorChar + label.getText()));
      if (!success)
        showError("Error renaming folder",
          "Folder could not be renamed to " + label.getText() + '.' + System.lineSeparator() +
            "Another folder might have the same name or you are using invalid symbols.");

      refreshDir();
    });
    vBox.getChildren().add(label);

    return vBox;
  }

  private void handleOnDragOver(DragEvent event, Node node) {
    // accept it only if it is not dragged from the same node and if it has a string data
    if (event.getGestureSource() != node &&
      event.getDragboard().hasFiles()) {
      event.acceptTransferModes(TransferMode.MOVE);
    }

    event.consume();
  }

  private void handleOnDragDropped(DragEvent event, File targetDir) {
    // if there is a file data on dragboard, read it and use it
    Dragboard db = event.getDragboard();
    boolean success = true;
    if (db.hasFiles()) {
      List<File> fileList = db.getFiles();
      System.out.println("try to move " + fileList.size() + " files " + db.getFiles());
      for (File file : fileList) {
        File target = new File(targetDir.getPath() + File.separatorChar + file.getName());
        System.out.println("Moving file " + file + " to " + target);
        if (!file.renameTo(target))
          success = false;
      }
    }
    if (success)
      refreshDir();
    else
      showError("Error moving file", "File could not be moved. " + System.lineSeparator() +
        "A file in the target directory might have the same name.");


    // let the source know whether the file was successfully transferred and used
    event.setDropCompleted(success);
  }

}
