package bot.states;

import bot.State;
import bot.StateContext;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;

import java.awt.event.KeyEvent;
import java.util.LinkedList;

public class MainState extends State {

  private LinkedList<Integer> pressedKeys;
  private LinkedList<Integer> recordKeys;

  private boolean recordKeysContained;

  public MainState(StateContext context) {
    super(context);

    pressedKeys = new LinkedList<>();
    recordKeys = context.getConfig().getRecordKeys();
  }

  @Override
  public void init() {
  }

  @Override
  public void destroy() {
  }

  @Override
  public Parent component() {
    HBox hBox = new HBox();
    hBox.setPrefSize(context.getWidth(), context.getBtnSize());
    hBox.setAlignment(Pos.TOP_RIGHT);

    hBox.getChildren().add(createButton("/icons/play.png", (event) -> context.setState(new CompareState(context))));
    hBox.getChildren().add(createButton("/icons/record.png", (event) -> context.setState(new RecordState(context))));
    hBox.getChildren().add(createButton("/icons/options.png", (event) -> context.setState(new OptionsState(context))));
    return hBox;
  }

  @Override
  public void keyPressed(KeyEvent keyEvent) {
    if (!pressedKeys.contains(keyEvent.getKeyCode()))
      pressedKeys.add(keyEvent.getKeyCode());
    if (pressedKeys.containsAll(recordKeys))
      recordKeysContained = true;
  }

  @Override
  public void keyReleased(KeyEvent keyEvent) {
    // remove as Integer, bc int would be seen as an index
    pressedKeys.remove(Integer.valueOf(keyEvent.getKeyCode()));

    // only switch the state, when the keys have been released again
    if (recordKeysContained && pressedKeys.isEmpty())
      context.setState(new RecordState(context));
  }

}
