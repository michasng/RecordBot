package bot.states;

import bot.State;
import bot.StateContext;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class OptionsState extends State {

  public OptionsState(StateContext context) {
    super(context);
  }

  @Override
  public void init() {
  }

  @Override
  public void destroy() {
  }

  @Override
  public Parent component() {
    HBox hBox = new HBox();
    hBox.setPrefSize(context.getWidth(), context.getBtnSize());
    hBox.setAlignment(Pos.TOP_RIGHT);
    hBox.getChildren().add(createButton("/icons/play_disabled.png", null));
    hBox.getChildren().add(createButton("/icons/record_disabled.png", null));
    hBox.getChildren().add(createButton("/icons/options.png", (event) -> context.setState(new MainState(context))));

    VBox vBox = new VBox();
    vBox.setPrefSize(context.getWidth(), context.getHeight());
    vBox.setAlignment(Pos.TOP_RIGHT);
    vBox.getChildren().add(hBox);
    vBox.getChildren().add(optionsPane());
    return vBox;
  }

  private Node optionsPane() {
    FlowPane flowPane = new FlowPane();
    flowPane.setMinSize(context.getBtnSize() * 4, context.getBtnSize() * 8);
    flowPane.setMaxSize(flowPane.getMinWidth(), flowPane.getMinHeight());
    flowPane.setId("menu");
    flowPane.setAlignment(Pos.TOP_RIGHT);
    flowPane.setPadding(new Insets(context.getBtnSize() / 4));
    flowPane.setHgap(context.getBtnSize() / 4);
    flowPane.setVgap(context.getBtnSize() / 4);

    Button setFolder = createButton("/icons/folder.png", (event) -> context.setState(new FolderState(context)));
    flowPane.getChildren().add(setFolder);

    Button quit = createButton("/icons/quit.png", (event) -> context.shutDown());
    flowPane.getChildren().add(quit);

    ToggleButton longPressAsOne = new ToggleButton("Long key-press as one");
    longPressAsOne.setAlignment(Pos.CENTER);
    longPressAsOne.setMinSize(
      context.getBtnSize() * 4 - flowPane.getPadding().getLeft() - flowPane.getPadding().getRight(),
      context.getBtnSize());
    longPressAsOne.setMaxSize(longPressAsOne.getMinWidth(), longPressAsOne.getMinHeight());
    longPressAsOne.setSelected(context.getConfig().isLongPressAsOne());
    longPressAsOne.setOnMouseClicked(event -> context.getConfig().setLongPressAsOne(longPressAsOne.isSelected()));
    longPressAsOne.setStyle("-fx-font-size: " + context.getBtnSize() * 0.24 + "px");
    flowPane.getChildren().add(longPressAsOne);

    Label recKeyCombo = new Label("Ctrl Alt R to Start Recording");
    recKeyCombo.setAlignment(Pos.CENTER);
    recKeyCombo.setMinSize(longPressAsOne.getMinWidth(), longPressAsOne.getMinHeight());
    recKeyCombo.setMaxSize(recKeyCombo.getMinWidth(), recKeyCombo.getMinHeight());
    recKeyCombo.setStyle("-fx-font-size: " + context.getBtnSize() * 0.18 + "px");
    flowPane.getChildren().add(recKeyCombo);

    Label stopKeyCombo = new Label("Ctrl Alt E to Stop");
    stopKeyCombo.setAlignment(Pos.CENTER);
    stopKeyCombo.setMinSize(recKeyCombo.getMinWidth(), recKeyCombo.getMinHeight());
    stopKeyCombo.setMaxSize(stopKeyCombo.getMinWidth(), stopKeyCombo.getMinHeight());
    stopKeyCombo.setStyle("-fx-font-size: " + context.getBtnSize() * 0.24 + "px");
    flowPane.getChildren().add(stopKeyCombo);

    return flowPane;
  }

}
