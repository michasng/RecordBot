package bot.states;

import bot.Record;
import bot.State;
import bot.StateContext;
import bot.UserAction;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.LinkedList;

public class PlayState extends State implements Runnable {

  private Record record;
  private boolean running;

  private LinkedList<Integer> pressedKeys;
  private LinkedList<Integer> escapeKeys;

  private LinkedList<Integer> unreleasedKeys;
  private LinkedList<Integer> unreleasedMouseBtns;

  public PlayState(StateContext context, Record record) {
    super(context);
    this.record = record;
  }

  @Override
  public void init() {
    pressedKeys = new LinkedList<>();
    escapeKeys = context.getConfig().getEscapeKeys();

    unreleasedKeys = new LinkedList<>();
    unreleasedMouseBtns = new LinkedList<>();

    run(); // already a new thread no need to change threads
  }

  @Override
  public void destroy() {
    running = false;
    releaseAll();
  }

  @Override
  public Parent component() {
    HBox hBox = new HBox();
    hBox.setPrefSize(context.getWidth(), context.getBtnSize());
    hBox.setAlignment(Pos.TOP_RIGHT);

    Label label = new Label("Playing " + record.getName());
    label.setPrefSize(5 * context.getBtnSize(), context.getBtnSize());
    label.setAlignment(Pos.CENTER);
    hBox.getChildren().add(label);
    Button button = createButton("/icons/pause.png", (event) -> context.setState(new MainState(context)));
    hBox.getChildren().add(button);
    hBox.getChildren().add(createButton("/icons/record_disabled.png", null));
    hBox.getChildren().add(createButton("/icons/options_disabled.png", null));
    return hBox;
  }

  @Override
  public void run() {
    running = true;
    Iterator<UserAction> userActions = record.actionsInterator();
    System.out.println("Start playing " + record.getName());

    // scale-factor to multiply the recorded image's size with, to get the
    // screen size
    double xScale = context.getWidth() / record.getImage().getWidth();
    double yScale = context.getHeight() / record.getImage().getHeight();

    while (userActions.hasNext()) {
      UserAction userAction = userActions.next();
      int[] metaInf = userAction.getMetaInf();
      try {
        Thread.sleep(userAction.getDelay());
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      if (!running) { // if supposed to stop
        System.out.println("Stopped playing " + record.getName() + " on demand");
        return;
      }
      if (userAction.deviceEquals(UserAction.MOUSE)) {
        // move the mouse to the recorded position, scaled to the
        // current position
        context.getRobot().mouseMove((int) (metaInf[0] * xScale), (int) (metaInf[1] * yScale));
        if (userAction.typeEquals(UserAction.PRESS)) {
          context.getRobot().mousePress(metaInf[2]);
          if (!unreleasedMouseBtns.contains(metaInf[2]))
            unreleasedMouseBtns.add(metaInf[2]);
        } else if (userAction.typeEquals(UserAction.RELEASE)) {
          context.getRobot().mouseRelease(metaInf[2]);
          unreleasedMouseBtns.remove(Integer.valueOf(metaInf[2]));
        }
      } else if (userAction.deviceEquals(UserAction.KEY)) {
        if (userAction.typeEquals(UserAction.PRESS))
          try {
            context.getRobot().keyPress(metaInf[0]);
            if (!unreleasedKeys.contains(metaInf[0]))
              unreleasedKeys.add(metaInf[0]);
          } catch (IllegalArgumentException e) {
            e.printStackTrace();
            context.setState(new MainState(context));
          }
        else if (userAction.typeEquals(UserAction.RELEASE))
          try {
            context.getRobot().keyRelease(metaInf[0]);
            unreleasedKeys.remove(Integer.valueOf(metaInf[0]));
          } catch (IllegalArgumentException e) {
            e.printStackTrace();
            context.setState(new MainState(context));
          }
      }
    }
    System.out.println("Done playing " + record.getName());
    context.setState(new CompareState(context));
  }

  public void releaseAll() {
    for (int mouseBtn : unreleasedMouseBtns)
      context.getRobot().mouseRelease(mouseBtn);

    for (int key : unreleasedKeys)
      context.getRobot().keyRelease(key);
  }

  @Override
  public void keyPressed(KeyEvent keyEvent) {
    if (!pressedKeys.contains(keyEvent.getKeyCode()))
      pressedKeys.add(keyEvent.getKeyCode());
    if (pressedKeys.containsAll(escapeKeys))
      context.setState(new MainState(context));
  }

  @Override
  public void keyReleased(KeyEvent keyEvent) {
    // remove as Integer, bc int would be seen as an index
    pressedKeys.remove(Integer.valueOf(keyEvent.getKeyCode()));
  }

}
