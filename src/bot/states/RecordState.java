package bot.states;

import java.awt.event.KeyEvent;
import java.util.LinkedList;

import org.jnativehook.mouse.NativeMouseEvent;

import bot.Record;
import bot.RecordBot;
import bot.State;
import bot.StateContext;
import bot.UserAction;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;

public class RecordState extends State {

  private Record record;
  private LinkedList<Integer> unreleasedKeys;
  private long lastTimeStamp;

  private LinkedList<Integer> escapeKeys;

  public RecordState(StateContext context) {
    super(context);
  }

  @Override
  public void init() {
    record = new Record(context.takeScreenShot(), context.getConfig().getDirectory());
    unreleasedKeys = new LinkedList<>();
    lastTimeStamp = System.currentTimeMillis();

    escapeKeys = context.getConfig().getEscapeKeys();
  }

  @Override
  public void destroy() {
  }

  @Override
  public Parent component() {
    HBox hBox = new HBox();
    hBox.setPrefSize(context.getWidth(), context.getBtnSize());
    hBox.setAlignment(Pos.TOP_RIGHT);

    hBox.getChildren().add(createButton("/icons/play_disabled.png", null));
    hBox.getChildren().add(createButton("/icons/stop.png", (event) -> {
      record.finalCut(); // cuts off the press of this button
      context.setState(new EditRecordState(context, record, MainState.class.getName()));
    }));
    hBox.getChildren().add(createButton("/icons/options_disabled.png", null));
    return hBox;
  }

  @Override
  public void nativeMousePressed(NativeMouseEvent nativeMouseEvent) {
    int button = RecordBot.toRobotMouseBtn(nativeMouseEvent.getButton());

    record.addAction(new UserAction(getTimeStamp(), UserAction.MOUSE, UserAction.PRESS, nativeMouseEvent.getX(),
      nativeMouseEvent.getY(), button));
  }

  @Override
  public void nativeMouseReleased(NativeMouseEvent nativeMouseEvent) {
    int button = RecordBot.toRobotMouseBtn(nativeMouseEvent.getButton());

    record.addAction(new UserAction(getTimeStamp(), UserAction.MOUSE, UserAction.RELEASE, nativeMouseEvent.getX(),
      nativeMouseEvent.getY(), button));
  }

  @Override
  public void keyPressed(KeyEvent keyEvent) {
    if (unreleasedKeys.contains(keyEvent.getKeyCode()))
      return;

    unreleasedKeys.add(keyEvent.getKeyCode());
    record.addAction(new UserAction(getTimeStamp(), UserAction.KEY, UserAction.PRESS, keyEvent.getKeyCode()));

    if (unreleasedKeys.containsAll(escapeKeys))
      ; // TODO: exit
  }

  @Override
  public void keyReleased(KeyEvent keyEvent) {
    unreleasedKeys.remove(Integer.valueOf(keyEvent.getKeyCode()));
    record.addAction(new UserAction(getTimeStamp(), UserAction.KEY, UserAction.RELEASE, keyEvent.getKeyCode()));
  }

  private long getTimeStamp() {
    long currentTimeStamp = System.currentTimeMillis();
    long delay = currentTimeStamp - lastTimeStamp;
    lastTimeStamp = currentTimeStamp;
    return delay;
  }

}
